import UIKit
import MapKit   //show map
import CoreLocation //show user's location

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager = CLLocationManager() //khởi tạo đối tượng quản lí sự thay đổi toạ độ.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest //bắt chính xác toạ độ
        locationManager.requestAlwaysAuthorization()//xin quyền cho phép lấy toạ độ
        locationManager.startUpdatingLocation() //cập nhật toạ độ
        mapView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //hàm xử lí mỗi khi thay đổi toạ độ
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation: CLLocation = locations[0] as CLLocation //toạ độ user đầu tiên
        let lat:CLLocationDegrees = userLocation.coordinate.latitude //kinh độ
        let long: CLLocationDegrees = userLocation.coordinate.longitude //vĩ độ
        let latDelta: CLLocationDegrees = 0.01
        let longDelta: CLLocationDegrees = 0.01
        let span: MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, long) // toạ độ user
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)//vùng trên bản đồ
        mapView.setRegion(region, animated: true)
        mapView.showsUserLocation = true //show user's current location
        // khong can dùng maker
        
        
    }
    
    
}
// custom maker
extension ViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let mkAnnoView: MKAnnotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
        mkAnnoView.image = UIImage(named: "icon9")
        mkAnnoView.frame.size = CGSize(width: 50, height: 50)
        
        return mkAnnoView
    }
}

